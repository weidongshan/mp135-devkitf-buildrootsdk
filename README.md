# STM32MP135DevKitF-BuildrootSDK 2023

``` c
mkdir STM32MP135 & cd STM32MP135
git clone https://e.coding.net/weidongshan/stm32mp135-devkitf/buildroot.git
git clone  https://e.coding.net/weidongshan/stm32mp135-devkitf/buildroot-external-st.git
cd buildroot 
make BR2_EXTERNAL=../buildroot-external-st stm32mp135d_devkitf_sd_defconfig
```